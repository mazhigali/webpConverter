package webpConverter

import (
	"image"
	"log"
	"os"

	"github.com/chai2010/webp"
	_ "image/jpeg"
	_ "image/png"
)

func Convert2Webp(pathOriginFile, pathWebpFile string, quality float32) bool {
	//Read image
	file, err := os.Open(pathOriginFile)
	if err != nil {
		log.Println(err)
		return false
	}
	defer file.Close()

	//fmt.Println("Reading ....")

	img, _, err := image.Decode(file)
	if err != nil {
		log.Println(err)
		return false
	}

	//fmt.Println("Converting...")

	if err := webp.Save(pathWebpFile, img, &webp.Options{Quality: quality}); err != nil {
		log.Println(err)
		return false
	}

	//log.Println("Save .webp ok", pathWebpFile)
	return true
}
