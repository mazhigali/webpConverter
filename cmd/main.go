package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"gitlab.com/mazhigali/webpConverter"
)

type Job struct {
	pathOriginFile string
	pathWebpFile   string
	quality        float32
}
type Result struct {
	job  Job
	done bool
}

var jobs = make(chan Job, 1)
var results = make(chan Result, 1)

func worker(wg *sync.WaitGroup) {
	for job := range jobs {
		output := Result{job, webpConverter.Convert2Webp(job.pathOriginFile, job.pathWebpFile, job.quality)}
		results <- output
	}
	wg.Done()
}
func createWorkerPool(noOfWorkers int) {
	var wg sync.WaitGroup
	for i := 0; i < noOfWorkers; i++ {
		wg.Add(1)
		go worker(&wg)
	}
	wg.Wait()
	close(results)
}

func result(done chan bool) {
	for result := range results {
		fmt.Printf("Job file %s converted to %s, sucessful %t\n", result.job.pathOriginFile, result.job.pathWebpFile, result.done)
	}
	done <- true
}

func main() {
	dir := flag.String("dir", "./", "path to dir for convert2Webp")
	reconvert := flag.Bool("reconvert", false, "reconvert to webp, default false")
	clean := flag.Bool("clean", false, "clean webp images which don't have a pair, default false")
	quality := flag.Int("quality", 70, "rate of quality 0-100, default 70")
	flag.Parse()

	go scanDir(*dir, *reconvert, *clean, float32(*quality))

	done := make(chan bool)
	go result(done)
	noOfWorkers := 3
	createWorkerPool(noOfWorkers)
	<-done
}

func scanDir(dir string, reconvert bool, clean bool, quality float32) {
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Fatal(err)
		}

		switch filepath.Ext(path) {
		case ".jpg", ".JPG", ".png", ".PNG", ".JPEG", ".jpeg":
			//fmt.Println(path)

			outPath := strings.Replace(path, filepath.Ext(path), ".webp", 1)

			//проверяем существует ли уже файл webp чтобы заново не конвертить его
			if _, err := os.Stat(outPath); err == nil {
				//fmt.Println("file exists", path)
				if reconvert == true {
					//если в опциях стоит что надо переконвертить то добавляем работу в воркер
					jobs <- Job{
						pathOriginFile: path,
						pathWebpFile:   outPath,
						quality:        quality,
					}
				}

			} else if os.IsNotExist(err) {
				//если файл не существует, то полюбому конвертим в webp
				//fmt.Println("file not exists", outPath)
				jobs <- Job{
					pathOriginFile: path,
					pathWebpFile:   outPath,
					quality:        quality,
				}

			} else {
				//если непредвиденая ошибка в связи с проверкой существования то вырубаем программу
				fmt.Println("file may or may not exist, i got error", path)
				log.Fatal(err)
			}
		case ".webp", ".WEBP":
			//если при запуске указали true для чистки директории от webp без пары
			if clean == true {
				remove := false

				checkExtensions := []string{".jpg", ".JPG", ".png", ".PNG", ".JPEG", ".jpeg"}
				for _, ext := range checkExtensions {
					checkPathFile := strings.Replace(path, filepath.Ext(path), ext, 1)

					//проверяем существует ли уже файл исходник webp
					if _, err := os.Stat(checkPathFile); err == nil {
						//fmt.Println("file exists", path)
						remove = false
						break

					} else if os.IsNotExist(err) {
						//если файл не существует то ставим метку на удаление webp файла и ищем дальше по разрешениям
						//fmt.Println("file not exists", outPath)
						remove = true
						continue

					} else {
						//если непредвиденая ошибка в связи с проверкой существования то вырубаем программу
						fmt.Println(" i got error checking file", path)
						log.Fatal(err)
					}

				}
				//если нету оригинала файла webp с другим расширением то удаляем этот webp -- это для битрикса
				if remove == true {
					//fmt.Println("deleting filt", path)
					err := os.Remove(path) // remove a single file
					fmt.Printf("Deleted file %s \n", path)
					if err != nil {
						fmt.Println("err removing file", err)
					}
				}
			}

		}

		return nil
	})
	if err != nil {
		panic(err)
	}
	close(jobs)
}
