.PHONY: build
build:
	go build -ldflags '-linkmode external -w -extldflags "-static"' -o ./cmd/webpConverter ./cmd

#.PHONY: test
#test:
	#go test -v -race -timeout 30s ./...

.DEFAULT_GOAL := build
